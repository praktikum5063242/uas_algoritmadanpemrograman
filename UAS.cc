#include <iostream>
using namespace std;

int main() {
    const int jumlahMahasiswa = 2; // Ubah sesuai dengan jumlah mahasiswa yang ingin diolah
    double nilai[jumlahMahasiswa], quiz[jumlahMahasiswa], absen[jumlahMahasiswa], uts[jumlahMahasiswa], uas[jumlahMahasiswa], tugas[jumlahMahasiswa];
    char Huruf_Mutu[jumlahMahasiswa];

    // Memasukkan nilai untuk setiap mahasiswa
    for (int i = 0; i < jumlahMahasiswa; ++i) {
        cout << "Nilai untuk Mahasiswa ke-" << i+1 << ":" << endl;
        cout << "Absen = ";
        cin >> absen[i];
        cout << "Tugas = ";
        cin >> tugas[i];
        cout << "Quiz = ";
        cin >> quiz[i];
        cout << "UTS = ";
        cin >> uts[i];
        cout << "UAS = ";
        cin >> uas[i];
    }

    // Menghitung nilai rata-rata dan menentukan huruf mutu untuk setiap mahasiswa
    for (int i = 0; i < jumlahMahasiswa; ++i) {
        nilai[i] = ((0.1 * absen[i]) + (0.2 * tugas[i]) + (0.3 * quiz[i]) + (0.4 * uts[i]) + (0.5 * uas[i])) / 2;

        if (nilai[i] > 85 && nilai[i] <= 100)
            Huruf_Mutu[i] = 'A';
        else if (nilai[i] > 70 && nilai[i] <= 85)
            Huruf_Mutu[i] = 'B';
        else if (nilai[i] > 55 && nilai[i] <= 70)
            Huruf_Mutu[i] = 'C';
        else if (nilai[i] > 40 && nilai[i] <= 55)
            Huruf_Mutu[i] = 'D';
        else if (nilai[i] >= 0 && nilai[i] <= 40)
            Huruf_Mutu[i] = 'E';
    }

    // Menampilkan nilai dan huruf mutu untuk setiap mahasiswa
    for (int i = 0; i < jumlahMahasiswa; ++i) {
        cout << "Nilai Mahasiswa ke-" << i+1 << ":" << endl;
        cout << "Absen = " << absen[i] << " UTS = " << uts[i] << endl;
        cout << "Tugas = " << tugas[i] << " UAS = " << uas[i] << endl;
        cout << "Quiz = " << quiz[i] << endl;
        cout << "Huruf Mutu : " << Huruf_Mutu[i] << endl << endl;
    }

    return 0;
}
